

using namespace std;

#include "BSManager.h"
#include "HellaManager.h"
#include "CounterHW.h"
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <termios.h>
#include <unistd.h>
#include <fcntl.h>
#include <ctime>
#include <cmath>
#include <iostream>

enum Code {
        FG_RED      = 31,
        FG_GREEN    = 32,
        FG_BLUE     = 34,
        FG_DEFAULT  = 39,
        BG_RED      = 41,
        BG_GREEN    = 42,
        BG_BLUE     = 44,
        BG_DEFAULT  = 49
    };

/* Keyboard Hit */
int kbhit(void)
{
  struct termios oldt, newt;
  int ch;
  int oldf;

  tcgetattr(STDIN_FILENO, &oldt);
  newt = oldt;
  newt.c_lflag &= ~(ICANON | ECHO);
  tcsetattr(STDIN_FILENO, TCSANOW, &newt);
  oldf = fcntl(STDIN_FILENO, F_GETFL, 0);
  fcntl(STDIN_FILENO, F_SETFL, oldf | O_NONBLOCK);

  ch = getchar();

  tcsetattr(STDIN_FILENO, TCSANOW, &oldt);
  fcntl(STDIN_FILENO, F_SETFL, oldf);

  if(ch != EOF)
  {
    ungetc(ch, stdin);
    return 1;
  }

  return 0;
}

/* Funcoes especificas do BSCounter */
int hellaCounter()
{
    class HellaManager hella;
    MsgNmGetHealth_t health;
    MsgNmStatus_t status;
    int in,out,c,flag_exit=0,ret = MSG_ERROR_NONE;
    time_t now = time(NULL);
    struct tm  tstruct;
    char  bufTime[80];
    int flag_counter = 0;


    /*Aguarda o dispositivo inicializar*/
    usleep(600000);

    hella.openConnection("localhost","192.168.234.212");
    if(hella.retrieveConnectionState() != MSG_ERROR_NONE)
    {
        cout << "Erro ao inicializar hella" << endl;
        return -1;
    }


    hella.setDateAndTime(time(NULL));
    hella.retrieveSystemHealth(&health);

    while(hella.retrieveConnectionState() == MSG_ERROR_NONE
          && ret == MSG_ERROR_NONE && flag_exit != 1)
    {
        /* Solicito os contadores */
        ret = hella.retrieveCounter(&in,&out);
        if(ret != MSG_ERROR_NONE)
            break;

        now = time(NULL);
        tstruct = *localtime(&now);
        strftime(bufTime, sizeof(bufTime), "%Y-%m-%d.%X", &tstruct);
        cout << "'\r[" << bufTime << "] - In: " << in << " out: " << out;
        cout.flush();

        /* Se alguma tecla for pressionada, uma acao sera efetuada */
        if (kbhit() != 0)
        {
            c=getchar();
             switch(c)
             {
                    /* Solicito o timestamp do Dispositivo */
                    case 't':

                    uint32_t timestamp;

                    hella.closeConnection();
                    hella.openConnection("localhost","192.168.234.212");

                    hella.retrieveDateAndTime(&timestamp);
                    cout << endl << "Me:" << now << " Device: " << timestamp << endl;

                    cout << endl << "Me:" << bufTime;
                    now = timestamp;
                    tstruct = *localtime(&now);
                    strftime(bufTime, sizeof(bufTime), "%Y-%m-%d.%X", &tstruct);
                    cout << " Device: " << bufTime << endl;


                    break;

                /* Reinicialiazo os contadores */
                case 'r':

                     cout << endl << "[" << bufTime << "] - Reseting counters...";

                     ret = hella.resetCounter();
                     if(ret != MSG_ERROR_NONE)
                        cout << "...Error!" << endl;
                     else
                        cout << "...Success!" << endl;
                     break;

                /* Exit */
                case 'q':
                    flag_exit = 1;
                    break;

                /* Solicito informacoes do dispositivo */
                case 'h':

                    /* Health */
                    memset(&health,0,sizeof(MsgNmGetHealth_t));
                    cout << endl <<"[" << bufTime << "] - Retrivieng System Health...";
                    ret = hella.retrieveSystemHealth(&health);
                    if(ret != MSG_ERROR_NONE)
                    {
                        cout << "...Error!" << endl;
                        break;
                    }
                    else
                       cout << "...Success!" << endl;

                    cout << "[" << bufTime << "] - System Health: ODS State: " << health.OSDState
                    << " Temp: " << health.temperature << " Voltage: " << health.voltage << endl;

                    /* Status */
                    memset(&status,0,sizeof(MsgNmStatus_t));
                    cout << "[" << bufTime << "] - Retrivieng System Status...";
                    ret = hella.retrieveSystemStatus(&status);
                    if(ret != MSG_ERROR_NONE)
                    {
                        cout << "...Error!" << endl;
                        break;
                    }
                    else
                       cout << "...Success!" << endl;

                    cout << "[" << bufTime << "] - System Status: Operating Mode: " << status.OperatingMode
                    << " Counting Mode: " << status.CountingMode << " Counting State: " << status.CountingState
                    << " CustomerID: " << status.CustomerID << " Error Flag: " << status.ErrorFlag << endl;

                    /* Uma simples Verificacao */
                    if(status.ErrorFlag != 0 || health.OSDState !=0 || status.CountingState == 0 )
                    {
                         cout << "[" << bufTime << "] - \033[1;41m### ALERT OSDSTATE: " << health.OSDState << " ErrorFlag: " << status.ErrorFlag <<
                         " Counting State: " << status.CountingState << " ###\033[0m" << endl << endl;
                    }
                    else
                        cout << "[" << bufTime << "] - \033[1;42m### SYSTEM HEALTH OK!###\033[0m" << endl;

                    break;

                /* Para / Inicializa contadores */
                case 's':

                    flag_counter = abs(flag_counter-1);
                    //cout << "Flag" << flag_counter << endl;
                    if(flag_counter == 0)
                        cout << endl << "[" << bufTime << "] - Counter Stopped...";
                    else if(flag_counter == 1)
                        cout << endl << "[" << bufTime << "] - Counter Started...";
                    else
                    {
                        cout << endl << "Invalid option" << endl;
                        break;
                    }

                    ret = hella.startStopCounter(flag_counter);
                    if(ret != MSG_ERROR_NONE)
                       cout << "...Error!" << endl;
                    else
                       cout << "...Success!" << endl;

                    break;

                /* Reboot */
                case 'R':
                    cout << endl << "[" << bufTime << "] - Rebooting...";
                    ret = hella.reboot(0);
                    if(ret != MSG_ERROR_NONE)
                       cout << "...Error!" << endl;
                    else
                       cout << "...Success!" << endl;

                    break;

                /* Halt */
                case 'H':
                    cout << endl << "[" << bufTime << "] - Halting...";
                    ret = hella.reboot(1);
                    if(ret != MSG_ERROR_NONE)
                       cout << "...Error!" << endl;
                    else
                       cout << "...Success!" << endl;

                    break;
                default:
                    break;
             }

        }
    }

    if(flag_exit != 1)
    {
        cout << "Exit with hella state: " << ret << endl;
        cout << "Exit with connection state: " << hella.retrieveConnectionState() << endl;
    }

    return MSG_ERROR_NONE;
}

/* Funcoes especificas do BSCounter */
void BSCounter()
{
    int i=0;
    BSManager bs;
    AlertEvent event;
    std::list <CounterEvent> events;
    std::list<CounterEvent>::iterator it;

    int in=0,out=0,ret;

    /* Contagem Total (Contadores) */
    cout << "Aguardando contadores...." << endl;
    ret = bs.retrieveCounter(&in,&out);
    if(ret != MSG_ERROR_NONE)
        cout << "Erro ao recuperar contadores: " << ret << endl;
    cout << "In " << in << " Out " << out << endl;

    /* Evento por evento */
    cout << "Aguardando Eventos...." << endl;
    ret = bs.retrieveCounterEvents(&events);
    if(ret != MSG_ERROR_NONE)
        cout << "Erro ao recuperar eventos: " << ret << endl;

    cout << "Listando Eventos...." << endl;
    for(it=events.begin(); it != events.end(); ++it)
    {
        cout << "#" << i++;
        cout << " Date: " << it->date << endl;
        cout << "Interval: " << it->interval << " Min" << endl;
        cout << "Object Id: " << it->objectID;
        cout << " DeviceId: " << it->deviceID;
        cout << " DeviceName: " << it->deviceName;
        cout << " ObjectType: " << it->ObjectType;
        cout << " Name: " << it->name << endl;
        cout << "StartTime: " << it->startTime;
        cout << " EndTime: " << it->endTime;
        cout << " Enters: " << it->enters;
        cout << " Exits: " << it->exits;
        cout << " Status: " << it->status << endl;

    }

    memset(&event,0,sizeof(AlertEvent));

    /* Evento de Alerta: Espera alguem cruzar e envia um alerta */
    cout << "Aguardando Alertas (Quando alguem cruzar a linha)...." << endl;
    ret = bs.waitForAlertEvent(&event);
    if(ret != MSG_ERROR_NONE)
        cout << "Erro ao recuperar Alertas: " << ret << endl;

    cout << "Date " << event.dateString << " In " << event.enters << " Out " <<
                    event.exits << " Type " << event.eventType << endl;



}

/*GenericCounter(int type=0)*/
/* Utiliza a Classe "Interface", para abstrair o uso de ambos os contadores */
/* OBS
    * Hella: Client(App) -> Servidor(HW)
        - App conecta no Hardware
    * BrickStream: Client(HW) -> Servidor(App)
        - Hardware se conecta na App, que fica na escuta
*/

void GenericCounter(int type=0)
{
    CounterHW *counter;
    int in=0,out=0,ret;
    if(type == 1)
    {
        cout << "### Hella Counter ###" << endl;
        counter = new HellaManager();
    }
    else
    {
        cout << "### BrickStream Counter ###" << endl;
        counter = new BSManager();
    }



    /* So tem efeito para o Hella, pois o BrickStream o conceito eh diferente
        No BrickStream, abrimos uma porta de escuto no retrieveCounter(),
        e esperamos o contador enviar os dados*/
    ret = counter->openConnection("localhost","192.168.234.212");
    if(ret != MSG_ERROR_NONE)
        cout << "Erro ao abrir conexao: " << ret << endl;

    ret = counter->retrieveCounter(&in,&out);
    if(ret != MSG_ERROR_NONE)
        cout << "Erro ao recuperar contadores: " << ret << endl;

    cout << "In " << in << " Out " << out << endl;

    /* So tem efeito para o Hella, pois o BrickStream o conceito eh diferente*/
    counter->closeConnection();

    delete counter;
}

int main()
{
    /*Hella Test*/
    //GenericCounter(1);
    //hellaCounter();

    /*BrickStream Test*/
    //GenericCounter(0);
    BSCounter();


    return 0;
}
